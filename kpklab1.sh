#!/bin/bash
echo "The neme of the program: checksum generation program."
echo "Program description: with this program you can calculate checksum of a file using the MD5 algorithm."
echo "Developer: Karshieva Polina (729-1)."
while [ 1 ]; do
echo "The current directory:"
pwd
echo "Enter name of the first file:"
read  THEFIRSTNAME
if [ -f $THEFIRSTNAME ]; then
        echo "Checksum:" 
        md5=$(md5sum $THEFIRSTNAME)
        echo $md5
else
        echo "File does not exist. Enter text:"
        read TEXT
        echo "File is creating..."
        echo $TEXT > $THEFIRSTNAME
        echo "Done."
        echo "Checksum:" 
        md5=$(md5sum $THEFIRSTNAME)
        echo $md5
fi
echo "Enter name of the second file:"
read THESECONDFILE
if [ -f $THESECONDFILE ]; then
        echo "File already exist."
        echo "Do you want to overwrite the file? Yes or no (y/n)?"
        read ITEM
        case $ITEM in
        y|Y) echo $md5 > $THESECONDFILE
        echo "Overwrite is done."
        ;;
        n|N) echo "Cancel operation."
        ;;
        *) echo "Cancel opation."
        ;;
        esac
else 
	echo "File does not exist."
        echo "File is creating..."
        echo $md5 > $THESECONDFILE
        echo "Done."
fi
echo "Do you want to repeat? Yes or no (y/n)?"
read ITEM
case $ITEM in
        y|Y) echo 
        ;;
        n|N) break
        ;;
        *) break
        ;;
esac
done
